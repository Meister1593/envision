use crate::{
    depcheck::{check_dependencies, DepType, Dependency, DependencyCheckResult},
    dependencies::common::{
        dep_cmake, dep_eigen, dep_gcc, dep_git, dep_glslang_validator, dep_gpp, dep_libdrm,
        dep_ninja, dep_openxr, dep_vulkan_headers, dep_vulkan_icd_loader,
    },
    linux_distro::LinuxDistro,
};
use std::collections::HashMap;

fn monado_deps() -> Vec<Dependency> {
    vec![
        dep_libdrm(),
        dep_openxr(),
        dep_vulkan_icd_loader(),
        dep_vulkan_headers(),
        Dependency {
            name: "wayland".into(),
            dep_type: DepType::SharedObject,
            filename: "libwayland-client.so".into(),
            packages: HashMap::from([
                (LinuxDistro::Arch, "wayland".into()),
                (LinuxDistro::Debian, "libwayland-dev".into()),
            ]),
        },
        dep_cmake(),
        dep_eigen(),
        dep_git(),
        dep_ninja(),
        dep_gcc(),
        dep_gpp(),
        Dependency {
            name: "glslc".into(),
            dep_type: DepType::Executable,
            filename: "glslc".into(),
            packages: HashMap::from([
                (LinuxDistro::Arch, "shaderc".into()),
                (LinuxDistro::Debian, "glslc".into()),
                (LinuxDistro::Fedora, "glslc".into()),
            ]),
        },
        dep_glslang_validator(),
    ]
}

pub fn check_monado_deps() -> Vec<DependencyCheckResult> {
    check_dependencies(monado_deps())
}

pub fn get_missing_monado_deps() -> Vec<Dependency> {
    check_monado_deps()
        .iter()
        .filter(|res| !res.found)
        .map(|res| res.dependency.clone())
        .collect()
}
